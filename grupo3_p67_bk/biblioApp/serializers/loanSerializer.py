from biblioApp.models.loan import Loan
from rest_framework import serializers

class LoanSerializer(serializers.ModelSerializer):
    class Meta:
        model = Loan
        fields = ['isbn', 'lastChangeDateinicialDate', 'finalDate']