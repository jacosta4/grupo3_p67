from django.contrib import admin
from .models.user import User
from .models.account import Account
from .models.book import Book
from .models.loan import Loan


# Register your models here.

admin.site.register(User)
admin.site.register(Account)
admin.site.register(Book)
admin.site.register(Loan)
