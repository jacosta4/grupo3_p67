from django.db import models
from .user import User
from .book import Book

class Loan(models.Model):

    id = models.AutoField(primary_key=True)
    user = models.ForeignKey(User, related_name='loan', on_delete=models.CASCADE)
    isbn = models.ForeignKey(Book, on_delete=models.CASCADE)
    inicialDate = models.DateTimeField()
    finalDate = models.DateTimeField()